/*
 * WonderScroll
 */


( function($){

    $.wonderScroll = function( element, container, options ){

        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var self = this;
        
        // Access to jQuery and DOM versions of element
        self.$element = $(element);
        self.element = element;
        
        // Add a reverse reference to the DOM object
        self.$element.data( "wonderScroll", self );
        

        var viewportWidth;
        var viewportHeight;



        /*
         * init()
         */
        self.init = function(){
            window.console && console.log('wonderScroll.init()');

            /*
            if( typeof( container ) === "undefined" || container === null ) container = "20px";
            self.container = container;
            self.options = $.extend({}, $.wonderScroll.defaultOptions, options);
            */

            // Put your initialization code here
            self.updateDimensions();
            self.initResize();

            
        };
        



        /*
         * initResize()
         */
         self.initResize = function(){
            window.console && console.log('wonderScroll.initResize()');

            $(window).resize(function() {
                self.updateDimensions();
            });

        };


        /*
         * updateDimensions()
         */
        self.updateDimensions = function(){
            document.body.style.overflow = "hidden";
            self.viewportWidth = $(window).width();
            self.viewportHeight = $(window).height();
            document.body.style.overflow = "";

            window.console && console.log( 'viewportWidth: ' + self.viewportWidth );
            window.console && console.log( 'viewportHeight: ' + self.viewportHeight );
        };


        /*
         * test()
         */
         self.test = function(){
            window.console && console.log('wonderScroll.test()');


        };
        


        // Run initializer
        self.init();
    };
    
    


    $.wonderScroll.defaultOptions = {
        container: "20px"
    };
    


    $.fn.wonderScroll = function( container, options ){

        return this.each(function(){
            ( new $.wonderScroll( this, container, options ));

		   // HAVE YOUR PLUGIN DO STUFF HERE
			
	
		   // END DOING STUFF

        });
    };
    
})(jQuery);