/*
 * wonderScroll jQuery plugin
 * Author: Mike Stone, mstone@innoceanusa.com
 */


;(function($) {

	// Change this to your plugin name. 
	var pluginName = 'wonderScroll';
	

	/*
	 * Plugin object constructor.
	 * Implements the Revealing Module Pattern.
	 */
	function Plugin(element, options) {
		window.console && console.log('wonderScroll constructor');


		// Private
 		// ---------------------------------------------------------


		var panelArray = [];
		var panelContainer;


		// References to DOM and jQuery versions of element.
		var el = element;
		var $el = $(element);

		// Extend default options with those supplied by user.
		options = $.extend({}, $.fn[pluginName].defaults, options);


		/*
		 * Initialize plugin.
		 */
		function init() {
			window.console && console.log('wonderScroll.init()');
			// Add any initialization logic here...

			hook('onInit');
		}


		/*
		 * setPanels()
		 */
		function setPanels() {
			window.console && console.log('wonderScroll.setPanelArray()');

			if( $('#wonderscroll').length > 0 ){

				// Set panelContainer
				panelContainer = $('#wonderscroll');

				// Loop through each panel and add to panelArray
				panelContainer.find('.wonderscroll-panel').each( function(index){

					var classList = $(this).attr('class').split(/\s+/);


				});


			} else {
				window.console && console.log('Error: No panel container defined with id of "wonderscroll".');
			}




			/*

				<div id="wonderscroll">
				    <div id="panel-1" class="wonderscroll-panel"></div>
				    <div id="panel-2" class="wonderscroll-panel"></div>
				    <div id="panel-3" class="wonderscroll-panel"></div>
				    <div id="panel-4" class="wonderscroll-panel"></div>
				    <div id="panel-5" class="wonderscroll-panel"></div>
				    <div id="panel-6" class="wonderscroll-panel"></div>
				    <div id="panel-7" class="wonderscroll-panel"></div>
				    <div id="panel-8" class="wonderscroll-panel"></div>
				    <div id="panel-9" class="wonderscroll-panel"></div>
				</div>

			*/

		}






		// Public
 		// ---------------------------------------------------------


		/*
		 * Example Public Method
		 */
		function fooPublic() {
			window.console && console.log('wonderScroll.fooPublic()');
			// Code goes here...
		}


		/*
		 * Get/set a plugin option.
		 * Get usage: $('#el').wonderScroll('option', 'key');
		 * Set usage: $('#el').wonderScroll('option', 'key', value);
		 */
		function option (key, val) {
			window.console && console.log('wonderScroll.option()');

			if (val) {
				options[key] = val;
			} else {
				return options[key];
			}
		}


		/*
		 * Destroy plugin.
		 * Usage: $('#el').wonderScroll('destroy');
		 */
		function destroy() {
			window.console && console.log('wonderScroll.destroy()');

			// Iterate over each matching element.
			$el.each(function() {
				var el = this;
				var $el = $(this);

				// Add code to restore the element to its original state...
				hook('onDestroy');

				// Remove Plugin instance from the element.
				$el.removeData('plugin_' + pluginName);
			});
		}


		/*
		 * Callback hooks.
		 * Usage: In the defaults object specify a callback function:
		 * hookName: function() {}
		 * Then somewhere in the plugin trigger the callback:
		 * hook('hookName');
		 */
		function hook(hookName) {
			window.console && console.log('wonderScroll.hook()');

			if (options[hookName] !== undefined) {

				// Call the user defined function.
				// Scope is set to the jQuery element we are operating on.
				options[hookName].call(el);
			}
		}


		/*
		 * Define Public Methods
		 */
		return {
			option: option,
			destroy: destroy,
			fooPublic: fooPublic
		};



		// Initialize
 		// ---------------------------------------------------------

		// Initialize the plugin instance.
		init();

	}



	/*
	 * Plugin definition.
	 */
	$.fn[pluginName] = function(options) {
		window.console && console.log(' ');
		window.console && console.log('-----');
		window.console && console.log('wonderScroll');


		// If the first parameter is a string, treat this as a call to
		// a public method.
		if ( typeof arguments[0] === 'string' ) {
			window.console && console.log('string true');

			var methodName = arguments[0];
			var args = Array.prototype.slice.call(arguments, 1);
			var returnVal;

			window.console && console.log('methodName: '+methodName);
			window.console && console.log('args: '+args);


			this.each( function() {

				// Check that the element has a plugin instance, and that
				// the requested public method exists.
				if ( $.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function' ) {
					// Call the method of the Plugin instance, and Pass it
					// the supplied arguments.
					returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);

				} else {
					throw new Error('Method ' +  methodName + ' does not exist on jQuery.' + pluginName);
				}
			});


			if (returnVal !== undefined){
				// If the method returned a value, return the value.
				return returnVal;

			} else {
				// Otherwise, returning 'this' preserves chainability.
				return this;
			}


		// If the first parameter is an object (options), or was omitted,
		// instantiate a new instance of the plugin.
		} else if ( typeof options === "object" || !options ) {
			window.console && console.log('string false');

			return this.each(function() {
				// Only allow the plugin to be instantiated once.
				if (!$.data(this, 'plugin_' + pluginName)) {
					// Pass options to Plugin constructor, and store Plugin
					// instance in the elements jQuery data object.
					$.data(this, 'plugin_' + pluginName, new Plugin(this, options));
				}
			});
		}
	};

	// Default plugin options.
	// Options can be overwritten when initializing plugin, by
	// passing an object literal, or after initialization:
	// $('#el').wonderScroll('option', 'key', value);
	$.fn[pluginName].defaults = {
		onInit: function() {},
		onDestroy: function() {}
	};

})(jQuery);